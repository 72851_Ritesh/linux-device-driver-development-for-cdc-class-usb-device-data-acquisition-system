#include <ESP8266WiFi.h>
#include <SoftwareSerial.h>
#include "ThingsBoard.h"
#include <string>


#define TOKEN               "ptG5S6XMlwC1XELkrGzX"
#define THINGSBOARD_SERVER  "demo.thingsboard.io"
#define SERIAL_DEBUG_BAUD   9600

WiFiClient espClient;
ThingsBoard tb(espClient);
int status = WL_IDLE_STATUS;

String myString; // complete message from arduino, which consist of sensor data
char rdata; // received characters
int   tds = 0 , wqi = 0;
float pH = 0.0 , ec = 0.0;
float temp = 0.0;



void setup() {

  Serial.begin(SERIAL_DEBUG_BAUD);
  //WiFi.begin(WIFI_AP, WIFI_PASSWORD);
  //InitWiFi();
  Serial.println("hello");
  pinMode(2, OUTPUT);
  /* Set ESP8266 to WiFi Station mode */
  WiFi.mode(WIFI_STA);
  /* start SmartConfig */
  WiFi.beginSmartConfig();

  /* Wait for SmartConfig packet from mobile */
  Serial.println("Waiting for SmartConfig.");
  while (!WiFi.smartConfigDone()) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("SmartConfig done.");

  /* Wait for WiFi to connect to AP */
  Serial.println("Waiting for WiFi");
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("WiFi Connected.");
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());
  digitalWrite(2, HIGH);
}
void loop() {

  if (Serial.available() <= 0)
  {
    Serial.println("not getting the data");
  }
  if (Serial.available() > 0 )
  {
    rdata = Serial.read();
    Serial.print(rdata);
    myString = myString + rdata;
    Serial.print(rdata);
    if ( rdata == '\n')
    {
      //  Serial.println(myString);
      String l = getValue(myString, ',', 0); //  for temp
      String m = getValue(myString, ',', 1); // for pH
      String n = getValue(myString, ',', 2); // for Tds

      String o = getValue(myString, ',', 3); // for EC
      String p = getValue(myString, ',', 4); // for WQI


      temp = l.toFloat();
      pH = m.toFloat();
      tds = n.toInt();

      ec = o.toFloat();
      wqi = p.toInt();

      //      if (WiFi.status() != WL_CONNECTED) {
      //        reconnect();
      //      }

      if (!tb.connected()) {
        // Connect to the ThingsBoard
        Serial.print("Connecting to: ");
        Serial.print(THINGSBOARD_SERVER);
        Serial.print(" with token ");
        Serial.println(TOKEN);
        if (!tb.connect(THINGSBOARD_SERVER, TOKEN)) {
          Serial.println("Failed to connect");
          return;
        }
      }

      Serial.println("Sending data...");

      tb.sendTelemetryFloat("Temperature", temp);
      tb.sendTelemetryInt("pH", pH);
      tb.sendTelemetryInt("TDS", tds);

      tb.sendTelemetryInt("EC", ec);
      tb.sendTelemetryInt("WQI", wqi);

      myString = "";
      // end new code
    }
  }
}


String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = { 0, -1 };
  int maxIndex = data.length() - 1;

  for (int i = 0; i <= maxIndex && found <= index; i++) {
    if (data.charAt(i) == separator || i == maxIndex) {
      found++;
      strIndex[0] = strIndex[1] + 1;
      strIndex[1] = (i == maxIndex) ? i + 1 : i;
    }
  }
  return found > index ? data.substring(strIndex[0], strIndex[1]) : "";
}
